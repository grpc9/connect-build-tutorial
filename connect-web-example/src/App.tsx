import { useState } from 'react'
import {
	createConnectTransport,
	createPromiseClient
} from '@bufbuild/connect-web'
import './App.css'

import { ElizaService } from '@buf/bufbuild_connect-web_bufbuild_eliza/buf/connect/demo/eliza/v1/eliza_connectweb'

const transport = createConnectTransport({
	baseUrl: 'https://demo.connect.build'
})

const client = createPromiseClient(ElizaService, transport)

function App() {
	const [inputValue, setInputValue] = useState('')

	return (
    <main className="App">
      <form>
				<input type="text" name="ask" id="ask" value={inputValue} onChange={e => setInputValue(e.target.value)} />
				<button type='submit'>Send</button>
			</form>
    </main>
  )
}

export default App
